#include <iostream>
#include <string>
#include "queue.h"

using namespace std;

int main() {
	CQUEUE song_list;

	song_list.enqueue("Bon Iver - Holocene");
	song_list.enqueue("James Blake - Limit to Your Love");
	song_list.enqueue("Frank Ocean - Swim Good");
	song_list.enqueue("The Jezabels - A Little Piece");


	//song_list.dequeue();
	//song_list.dequeue();
	//song_list.dequeue();

	song_list.enqueue("Munchi - Hope (REMIX)");
	
	song_list.print();

	//CQUEUE blah_list(song_list);
	
	//blah_list.print();

	return 0;
}