#include <iostream>
#include <string>

using namespace std;

class qnode {
public:
	string name;
	qnode *prev;
	qnode *next;
};

class CQUEUE { 
public:
	CQUEUE();
	CQUEUE(const CQUEUE &);
	~CQUEUE();
	void enqueue(string);
	void dequeue();
	bool is_empty();
	void print();
private:
	qnode *front;
};