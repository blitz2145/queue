#include <iostream>
#include <string>
#include "queue.h"

using namespace std;

CQUEUE::CQUEUE() {
	front = NULL;
}

CQUEUE::CQUEUE(const CQUEUE &rhs) {
	qnode *p = rhs.front;
	front = NULL;

	if (rhs.front == NULL) {
		cout<<"The Given Queue is Empty!"<<endl;
	} else {
		while (p != rhs.front->prev) {
			enqueue(p->name);
			p = p->next;
		}
		enqueue(p->name);
	}
}

CQUEUE::~CQUEUE() {
	while (front != NULL) {
		dequeue();
	}
}

void CQUEUE::dequeue() {
	if (is_empty()) {
		cout<<"Cannot dequeue, queue is empty"<<endl;
	} else if (front == front->prev) {
		delete front;
		front = NULL;
	} else {
		qnode *tmp = front;
		front = front->next;
		front->prev = tmp->prev;
		delete tmp;
	}
}

void CQUEUE::enqueue(string val) {
	if (is_empty()) {
		//Set front and rear to 0 and enqueue value.
		front = new qnode;
		front->name = val;
		front->next = front;
		front->prev = front;
	} else {
		//Enqueue at the rear of the queue.
		qnode *tmp = front->prev;

		tmp->next = new qnode;
		tmp->next->prev = tmp;
		tmp->next->name = val;
		tmp->next->next = front;
		front->prev = tmp->next;
	}
}

bool CQUEUE::is_empty() {
	return (front == NULL);
}

void CQUEUE::print() {
	qnode *p = front;

	if (is_empty()) {
		cout<<"The Queue is empty!"<<endl;
	} else if (p == front->prev) {
		cout<<p->name<<endl;
	} else {
		while (p != front->prev) {
			cout<<p->name<<endl;
			p = p->next;
		}
		cout<<p->name<<endl;
	}
}